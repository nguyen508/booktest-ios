//
//  GetListBookAction.swift
//  BookTest
//
//  Created by ph quang on 08/11/2023.
//

import Foundation
import Base_swift

class GetListBookAction: Action<GetListBookAction.RV, DataPage<Book>> {
    
    override func onExecute(input: RV) throws -> DataPage<Book>? {
        return try RepoFactory.getBookRepo().getListBook(pageOffset: input.pageOffset)
    }
    
    struct RV {
        var pageOffset: Int = -1
    }

}

//
//  PBookRepo.swift
//  BookTest
//
//  Created by ph quang on 08/11/2023.
//

import Foundation

protocol PBookRepo: PRepo {
    func getListBook(pageOffset: Int) throws -> DataPage<Book>
}

//
//  RepoFactory.swift
//  BookTest
//
//  Created by ph quang on 08/11/2023.
//

import Foundation

class RepoFactory {

    class func getBookRepo() -> PBookRepo {
        return BookRepoImpl()
    }
}

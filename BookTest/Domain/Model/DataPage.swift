//
//  DataPage.swift
//  BookTest
//
//  Created by ph quang on 08/11/2023.
//

import Foundation
import Base_swift

class DataPage<T> {
    var dataList: [T] = []
    var pageOffset: Int = -1
    var total: Int = 0
}

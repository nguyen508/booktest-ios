//
//  Book.swift
//  BookTest
//
//  Created by ph quang on 08/11/2023.
//

import Foundation

class Book {
    var title: String?
    var displayTitle: String?
    var author: String?
    var description: String?
    var publisher: String?
    var rank: Int?

}

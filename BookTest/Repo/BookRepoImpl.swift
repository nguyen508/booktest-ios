//
//  BookRepoImpl.swift
//  BookTest
//
//  Created by ph quang on 08/11/2023.
//

import Foundation
import Base_swift

class BookRepoImpl : PBookRepo {
    func getListBook(pageOffset: Int) throws -> DataPage<Book> {
        let apiService = ApiServiceImpl()
        let request = BookHistoryRequest(offset: pageOffset)
        let response: BookHistoryResponse = try apiService.request(request: request)
        
        if let rs = response.results {
            let converter = NonNilArrayConverter<BookData, Book>(itemConverter: BookDataToBook())
            let pageData = DataPage<Book>()
            pageData.dataList = converter.convert(input: rs)
            pageData.pageOffset = pageOffset
            pageData.total = response.numResults ?? 0
            
            return pageData
        } else {
            throw BaseError(code: response.status, message: nil)
        }
    }
}

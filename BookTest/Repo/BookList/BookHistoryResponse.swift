//
//  BookHistoryResponse.swift
//  BookTest
//
//  Created by ph quang on 08/11/2023.
//

import Foundation

class BookHistoryResponse: BaseResponseData<[BookData]> {
    
    var copyright: String?
    var numResults: Int?
    
    
    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
        let container = try decoder.container(keyedBy: CodingKeys.self)
        copyright = try container.decodeIfPresent(String.self, forKey: .copyright)
        numResults = try container.decodeIfPresent(Int.self, forKey: .numResults)
    }
    
    enum CodingKeys: String, CodingKey {
        case copyright = "copyright"
        case numResults = "num_results"
        
    }
    
}

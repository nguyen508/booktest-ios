//
//  BookDataToBook.swift
//  BookTest
//
//  Created by ph quang on 08/11/2023.
//

import Foundation

class BookDataToBook: SimpleConverter<BookData, Book> {
    override func convert(input: BookData) -> Book? {
        let output = Book()
        
        output.title = input.title
        output.displayTitle = input.ranksHistory?.first?.displayName
        output.author = input.author
        output.description = input.des
        output.publisher = input.publisher
        output.rank = input.ranksHistory?.first?.rankLastWeek
        
        return output
    }
}

//
//  Isbn.swift
//  BookTest
//
//  Created by ph quang on 08/11/2023.
//

import Foundation

struct Isbn: Codable {
    
    let isbn10: String?
        
    let isbn13: String?
    
    enum CodingKeys: String, CodingKey {
        case isbn10 = "isbn10"
        case isbn13 = "isbn13"
        
    }
}

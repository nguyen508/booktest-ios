//
//  BookData.swift
//  BookTest
//
//  Created by ph quang on 08/11/2023.
//

import Foundation

struct BookData: Codable {
    
    let ageGroup: String?
        
    let author: String?
        
    let contributor: String?
    
    let contributorNote: String?
    
    let des: String?
    
    let isbns: [Isbn?]?
    
    let price: String?
    
    let publisher: String?
    
    let ranksHistory: [RanksHistory]?
    
    let reviews: [Review?]?
    
    let title: String?
    
    enum CodingKeys: String, CodingKey {
        case ageGroup = "age_group"
        case author = "author"
        case contributor = "contributor"
        case contributorNote = "contributor_note"
        case des = "description"
        case isbns = "isbns"
        case price = "price"
        case publisher = "publisher"
        case ranksHistory = "ranks_history"
        case reviews = "reviews"
        case title = "title"
    }
}

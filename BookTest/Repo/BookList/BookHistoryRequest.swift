//
//  BookHistoryRequest.swift
//  BookTest
//
//  Created by ph quang on 08/11/2023.
//

import Alamofire

class BookHistoryRequest: BaseRequest {
    
    var baseUrl: String = ApiConfig.baseUrl
    var path: String
    
    var headers: HTTPHeaders?
    
    var method: HTTPMethod = .get
    
    var parameters: Parameters?
    
    init(offset: Int) {
        self.path = "books/v3/lists/best-sellers/history.json"
        self.parameters = ["api-key": ApiConfig.apiKey, "offset": offset]
    }
}

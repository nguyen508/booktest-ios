//
//  Review.swift
//  BookTest
//
//  Created by ph quang on 08/11/2023.
//

import Foundation


struct Review: Codable {
    
    let articleChapterLink: String?
    let bookReviewLink: String?
    let firstChapterLink: String?
    let sundayReviewLink: String? 

    enum CodingKeys: String, CodingKey {
        case articleChapterLink = "article_chapter_link"
        case bookReviewLink = "book_review_link"
        case firstChapterLink = "first_chapter_link"
        case sundayReviewLink = "sunday_review_link"
    }
}

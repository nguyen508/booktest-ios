//
//  RanksHistory.swift
//  BookTest
//
//  Created by ph quang on 08/11/2023.
//

import Foundation

struct RanksHistory: Codable {
    
   let asterisk: Int?
   
   let bestsellersDate: String?

   let dagger: Int?

   let displayName: String?

   let listName: String?

   let primaryIsbn10: String?

   let primaryIsbn13: String?

   let publishedDate: String?

   let rank: Int?

   let rankLastWeek: Int?

   let weeksOnList: Int?
    
    enum CodingKeys: String, CodingKey {
        case asterisk = "asterisk"
        case bestsellersDate = "bestsellers_date"
        case dagger = "dagger"
        case displayName = "display_name"
        case listName = "list_name"
        case primaryIsbn10 = "primary_isbn10"
        case primaryIsbn13 = "primary_isbn13"
        case publishedDate = "published_date"
        case rank = "rank"
        case rankLastWeek = "rank_last_week"
        case weeksOnList = "weeks_on_list"
    }
}

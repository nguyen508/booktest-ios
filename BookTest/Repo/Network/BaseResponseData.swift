//
//  BaseApiData.swift
//
//  Created by ph quang on 07/11/2023.
//
//

class BaseResponseData<D: Codable>: Codable {
    
    enum CodingKeys: String, CodingKey {
        
        case status = "status"
        case results = "results"
        
    }
    
    var status: String?

    var results: D?
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        results = try container.decodeIfPresent(D.self, forKey: .results)
        
        do {
            let value = try container.decode(Int.self, forKey: .status)
            status = String(value)
        } catch DecodingError.typeMismatch {
            status = try container.decode(String.self, forKey: .status)
        }
    
    }
    
    func isSuccess() -> Bool {
        return "OK" == status
    }
}

//
//  BaseApiService.swift
//
//  Created by ph quang on 07/11/2023.
//
//

import Alamofire

protocol PApiService {
    func request<T: Codable, R: BaseResponseData<T>>(request: URLRequestConvertible) throws -> R
}

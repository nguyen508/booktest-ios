//
//  BaseApiServiceImpl.swift
//
//  Created by ph quang on 07/11/2023.
//
//

import Alamofire
import Base_swift

class ApiServiceImpl: PApiService {
    
    
    private(set) var session: Session!
    
    init() {
        session = initSession()
    }
    
    func initSession() -> Session {
        Session(interceptor: Interceptor())
    }
    
    
    func request<T, R>(request: Alamofire.URLRequestConvertible) throws -> R where T : Decodable, T : Encodable, R : BaseResponseData<T> {
        let semaphore = DispatchSemaphore(value: 0)
        var data: R!
        var error: Error?
        session.request(request).validate().response(queue: .global()) { response in
            print("URL Request: \(request.urlRequest)")
            if let responseData = response.data {
                do {
                    let decoder = JSONDecoder()
                    data = try decoder.decode(R.self, from: responseData)
                } catch let e {
                    error = BaseError(code: nil, message: e.localizedDescription)
                }
            } else {
                error = BaseError(code: nil, message: response.error?.localizedDescription)
            }
            semaphore.signal()
        }
        _ = semaphore.wait(timeout: .distantFuture)
        if error != nil {
            throw error!
        }
        return data
    }
    
    
    
   
}

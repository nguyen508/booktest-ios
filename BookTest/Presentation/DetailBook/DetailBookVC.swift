//
//  DetailBookVC.swift
//  Test
//
//  Created by ph quang on 07/11/2023.
//

import UIKit
import Base_swift

class DetailBookVC: JetUIViewController<DetailBookView> {

    var book: Book?
    
    override func onPresenterReady() {
        super.onPresenterReady()
        title = "Detail Book"
        mvpView.showBookDetail(book: book)
    }

}

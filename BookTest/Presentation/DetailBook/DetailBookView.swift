//
//  DetailBookView.swift
//  Test
//
//  Created by ph quang on 07/11/2023.
//

import UIKit
import Base_swift

class DetailBookView: MVPUIView {
    
    @IBOutlet weak var lbDetailTitle: UILabel!
    
    @IBOutlet weak var lbDetailDesc: UILabel!
    
    @IBOutlet weak var lbDetailPublisher: UILabel!
    
    @IBOutlet weak var lbDetailRank: UILabel!
    
    
    override func onInitView() {
        super.onInitView()
    }
    
    func showBookDetail(book: Book?) {
        lbDetailTitle.text = book?.title
        lbDetailDesc.text = book?.description
        lbDetailPublisher.text = book?.publisher
        lbDetailRank.text = "\(book?.rank ?? 0)"
    }

}

//
//  AddBookView.swift
//  BookTest
//
//  Created by ph quang on 09/11/2023.
//

import UIKit
import Base_swift

class AddBookView: MVPUIView {
    @IBOutlet weak var txtBookTitle: UITextField!
    
    @IBOutlet weak var txtBookAuthor: UITextField!
    
    @IBOutlet weak var txtBookPublisher: UITextField!
    @IBOutlet weak var txtBookDescription: UITextField!
    
    
    override func onInitView() {
        super.onInitView()
    }
    @IBAction func addBookAction(_ sender: Any) {
        var book = Book()
        book.title = txtBookTitle.text
        book.author = txtBookAuthor.text
        book.publisher = txtBookPublisher.text
        book.description = txtBookDescription.text
        
        presenter?.executeCommand(command: AddBookCmd(book: book))
    }
    
    struct AddBookCmd: PCommand {
        let book: Book
    }
}

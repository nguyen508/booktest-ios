//
//  AddBookVC.swift
//  BookTest
//
//  Created by ph quang on 09/11/2023.
//

import UIKit
import Base_swift

class AddBookVC: JetUIViewController<AddBookView> {
    
    override func onPresenterReady() {
        title = "Add Book"
    }
    
    override func onExecuteCommand(command: PCommand) throws {
        try super.onExecuteCommand(command: command)
        
        if let cmd = command as? AddBookView.AddBookCmd {
            backToListBook(cmd.book)
        }
    }
    
    private func backToListBook(_ book: Book) {
        let _ = self.returnData(data: book)
        finish()
    }
}

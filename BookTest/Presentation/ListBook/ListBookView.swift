//
//  ListBookView.swift
//  Test
//
//  Created by ph quang on 06/11/2023.
//

import UIKit
import Base_swift

class ListBookView: MVPUIView {

    @IBOutlet weak var tblBooks: LoadMoreTableView!
    
    private var dataSource: ListBookDataSource = ListBookDataSource(data: nil)
    
    override func onInitView() {
        super.onInitView()
        
        dataSource.registerDataSource(tableView: tblBooks)
        dataSource.presenter = presenter
        
        tblBooks.addLoadMore(action: { [weak self] in
            guard let self = self else {return}
            print("Execute LoadMore")
            self.presenter?.executeCommand(command: LoadMoreCmd())
        })
        
    }
    
    
    func addBooks(_ bookPage: DataPage<Book>) {
        dataSource.add(data: bookPage.dataList)
        tblBooks.setLoadMoreEnable(bookPage.pageOffset < bookPage.total)
        tblBooks.reloadData()
        
    }
    
    func addBookItem(book: Book) {
        dataSource.addBook(book: book)
        tblBooks.reloadData()
    }
    
    struct LoadMoreCmd: PCommand {}

}

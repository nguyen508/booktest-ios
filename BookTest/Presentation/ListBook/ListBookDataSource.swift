//
//  ListBookDataSource.swift
//  BookTest
//
//  Created by ph quang on 09/11/2023.
//

import Foundation
import UIKit
import Base_swift

class ListBookDataSource: BaseTableViewDataSource {
    weak var presenter: PPresenter?
    
    override func initCell(cell: BaseTableCell, indexPath: IndexPath) {
        
        if let cell = cell as? BookCell {
            cell.didSeletectBookListener = { [weak self] book in
                self?.presenter?.executeCommand(command: SelectItemCmd(item: book))
            }
        }
    }
    
    override func getCellType(at position: Int) -> AnyClass {
        return BookCell.self
    }
    
    override func getRegisteredCellTypes() -> [AnyClass] {
        return [BookCell.self]
    }
    
    override func getCount() -> Int {
        return (data?.count ?? 0)
    }
    
    override func getDataForRow(position: Int) -> Any? {
        return data?[position]
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCell.EditingStyle.delete {
            removeItem(indexPath.row)
            tableView.reloadData()
        }
    }
    
    func addBook(book: Book) {
        data?.insert(book, at: 0)
    }
    
    private func removeItem(_ index: Int) {
        data?.remove(at: index)
        reset(data: data)
    }
    
    
    struct SelectItemCmd: PCommand {
        let item: Book
    }
    
}

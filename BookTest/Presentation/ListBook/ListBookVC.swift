//
//  ListBookVC.swift
//  Test
//
//  Created by ph quang on 06/11/2023.
//

import UIKit
import Base_swift

class ListBookVC: JetUIViewController<ListBookView> {
    
    
    private var pageIndex = 0
    private var pageSize = 20
    
    override func onPresenterReady() {
        super.onPresenterReady()
        title = "List Book"
        createAddButton()
        getListBook(pageIndex)
    }
    
    override func onExecuteCommand(command: PCommand) throws {
        try super.onExecuteCommand(command: command)
        
        if command is ListBookView.LoadMoreCmd {
            loadMorePage()
        }
        
        if let cmd = command as? ListBookDataSource.SelectItemCmd {
            navigateToDetail(item: cmd.item)
        }
    }
    
    private func getListBook(_ pageIndex: Int) {
        let callback = JetActionCallback<DataPage<Book>>(context: self, onSuccess: {[weak self] data in
            guard let self = self, let d = data else {return}
            
            self.mvpView.addBooks(d)
            
        })
        
        actionManager.execute(action: GetListBookAction(),
                              input: GetListBookAction.RV(pageOffset: pageIndex*pageSize),
                              callback: callback,
                              scheduler: AsyncScheduler(inConcurrent: true))
        .run()
    }
    
    
    private func loadMorePage() {
        pageIndex += 1
        getListBook(pageIndex)
    }
    
    private func navigateToDetail(item: Book) {
        let vc = DetailBookVC()
        vc.book = item
        navi(to: vc)
    }
    
    private func createAddButton() {
        let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addAction))
        navigationItem.rightBarButtonItem = addButton
    }
    
    @objc func addAction() {
        let vc = AddBookVC()
        
        navi(to: vc).with { [weak self] dataDict in
            guard let self = self, let dict = dataDict else {return}
            let newBook: Book = self.getReturnData(dataDic: dict)
            self.mvpView.addBookItem(book: newBook)
            
        }
    }
    
}

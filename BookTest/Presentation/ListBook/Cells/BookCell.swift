//
//  BookCell.swift
//  BookTest
//
//  Created by ph quang on 08/11/2023.
//

import UIKit
import Base_swift

class BookCell: TableCell<Book> {
    @IBOutlet weak var lbAuthor: UILabel!
    
    @IBOutlet weak var lbTitle: UILabel!
    
    var didSeletectBookListener: ((Book) -> Void)?
    
    override func initCell() {
        super.initCell()
        setOnClickedListener { [weak self] in
            guard let self = self else {return}
            self.didSeletectBookListener?(self.currentData as! Book)
        }
    }
    override func onBind(data: Book?) {
        super.onBind(data: data)
        lbTitle.text = data?.title
        lbAuthor.text = data?.author
        
    }
    
}

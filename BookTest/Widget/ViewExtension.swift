//
//  ViewExtension.swift
//  TestApp
//
//  Created by ph quang on 18/11/2021.
//

import Foundation
import UIKit

protocol Highlightable {
    func show(highlight: Bool)
}

class OpacityHighlight: Highlightable {
    
    private weak var view: UIView?
    private var highlightAlpha: CGFloat
    private var normalAlpha: CGFloat
    private var originalColor: UIColor? = nil
    private var usingAlpha = true
    
    init(view: UIView, highlightAlpha: CGFloat) {
        self.view = view
        if view.backgroundColor == UIColor.clear
            || view.backgroundColor == UIColor.white {
            usingAlpha = false
        }
        if usingAlpha {
            self.normalAlpha = view.alpha
            self.highlightAlpha = highlightAlpha
        } else {
            originalColor = view.backgroundColor
            self.normalAlpha = -1
            self.highlightAlpha = -1
        }
    }
    
    func show(highlight: Bool) {
        if highlight {
            if usingAlpha {
                self.view?.alpha = self.highlightAlpha
            } else {
                self.view?.backgroundColor = UIColor("E0E0E0")
            }
        } else {
            if usingAlpha {
                self.view?.alpha = normalAlpha
            } else {
                self.view?.backgroundColor = originalColor
            }
        }
    }
}

final class OnClickListener: UITapGestureRecognizer, UIGestureRecognizerDelegate {
    var action: ()-> Void
    
    private var highlights = [Highlightable]()
    private var downPoint: CGPoint? = nil
    
    init(_ action: @escaping () -> Void) {
        self.action = action
        super.init(target: nil, action: nil)
        self.addTarget(self, action: #selector(execute))
        self.delegate = self
    }
    
    deinit {
        print("OnClickListener deinit")
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent) {
        super.touchesBegan(touches, with: event)
        showHighlight(isHighlight: true)
    
        if let first = touches.first {
            downPoint = first.location(in: self.view)
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent) {
        super.touchesEnded(touches, with: event)
        showHighlight(isHighlight: false)
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent) {
        super.touchesCancelled(touches, with: event)
        showHighlight(isHighlight: false)
        
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent) {
        super.touchesMoved(touches, with: event)
        if let t = touches.first {
            if downPoint != nil {
                let curr = t.location(in: self.view)
                if abs(curr.y - downPoint!.y) > 2 {
                    showHighlight(isHighlight: false)
                    downPoint = nil
                }
            }
        }
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    @objc private func execute() {
        self.action()
    }
    
    func addHighlightable(highlight: Highlightable) {
        highlights.append(highlight)
    }
    
    private func showHighlight(isHighlight: Bool) {
        for hl in highlights {
            hl.show(highlight: isHighlight)
        }
    }
}

extension UIView {
    func setOnClickedListener(_ action: @escaping () -> Void) {
        var hasSet = false
        if let gestures = self.gestureRecognizers {
            for gesture in gestures {
                if gesture is OnClickListener {
                    //self.removeGestureRecognizer(gesture)
                    (gesture as! OnClickListener).action = action
                    hasSet = true
                    break
                }
            }
        }
        
        if !hasSet {
            self.isUserInteractionEnabled = true
            let onClickedGes = OnClickListener(action)
            onClickedGes.addHighlightable(highlight: OpacityHighlight(view: self, highlightAlpha: 0.5))
            self.addGestureRecognizer(onClickedGes)
        }
    }
}

extension UIView {
    @IBInspectable var strockColor: UIColor? {
        get {
            guard let cgColor = self.layer.borderColor else {
                return nil
            }
            return UIColor(cgColor: cgColor)
        }
        
        set {
            if newValue != nil {
                self.layer.borderColor = newValue!.cgColor
            }
        }
    }
    
    @IBInspectable var strockWidth: CGFloat {
        get {
            return self.layer.borderWidth
        }
        
        set {
            if newValue > 0 {
                self.layer.borderWidth = newValue
            }
        }
    }
    
    @IBInspectable var radius: CGFloat {
        get {
            return self.layer.cornerRadius
        }
        
        set {
            if newValue > 0 {
                self.layer.cornerRadius = newValue
                self.layer.masksToBounds = true
            }
        }
    }
}

extension UIView {
    func removeAllConstraints() {
        var parent = self.superview
        while let parentView = parent {
            for const in parentView.constraints {
                if let first = const.firstItem as? UIView, first == self {
                    parentView.removeConstraint(const)
                }
                
                if let second = const.secondItem as? UIView, second == self {
                    parentView.removeConstraint(const)
                }
            }
            parent = parentView.superview
        }
        
        self.removeConstraints(constraints)
        self.translatesAutoresizingMaskIntoConstraints = true
    }
}

extension UIImage {

    func with(_ insets: UIEdgeInsets) -> UIImage {
        let targetWidth = size.width + insets.left + insets.right
        let targetHeight = size.height + insets.top + insets.bottom
        let targetSize = CGSize(width: targetWidth, height: targetHeight)
        let targetOrigin = CGPoint(x: insets.left, y: insets.top)
        let format = UIGraphicsImageRendererFormat()
        format.scale = scale
        let renderer = UIGraphicsImageRenderer(size: targetSize, format: format)
        return renderer.image { _ in
            draw(in: CGRect(origin: targetOrigin, size: size))
        }.withRenderingMode(renderingMode)
    }
}

//MARK: - Constraints
extension UIView {
    public func fill(parent view: UIView, constant: UIEdgeInsets = .zero){
        self.translatesAutoresizingMaskIntoConstraints = false
        self.leftAnchor.constraint(equalTo: view.leftAnchor, constant: constant.left).isActive = true
        self.topAnchor.constraint(equalTo: view.topAnchor, constant: constant.top).isActive = true
        view.rightAnchor.constraint(equalTo: self.rightAnchor, constant: constant.right).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: constant.bottom).isActive = true
        self.layoutIfNeeded()
    }
}

extension UIColor {
    
    convenience init(_ hex: UInt) {
        self.init(
            red: CGFloat((hex & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((hex & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(hex & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    convenience init(_ hexString: String) {
        let hexString: String = hexString.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        let scanner           = Scanner(string: hexString)
        
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        
        var color:UInt32 = 0
        scanner.scanHexInt32(&color)
        
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        
        self.init(red:red, green:green, blue:blue, alpha:1)
    }
    
    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        
        getRed(&r, green: &g, blue: &b, alpha: &a)
        
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        
        return NSString(format:"#%06x", rgb) as String
    }
}

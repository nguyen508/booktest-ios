//
//  LoadMoreTableView.swift
//  Finhay
//
//  Created by ph quang on 18/04/2022.
//

import UIKit

open class LoadMoreTableView: UITableView {

    private var loadMoreView: LoadMoreView!
    private var originContentInset: CGPoint?
    private let loadMoreDefaultHeight: CGFloat = 50
    
    public var pullToRefreshListener: (() -> Void)?

    public override func reloadData() {
        
        print("Reload Data")
        stopLoadMore()
        stopPullToRefresh()
        super.reloadData()
    }

    public func addPullToRefresh() {

        refreshControl = UIRefreshControl()
        refreshControl!.attributedTitle = NSAttributedString(string: "Kéo xuống để làm mới", attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
        refreshControl!.addTarget(self, action: #selector(self.refreshAction), for: .valueChanged)
    }
    
    public func startPullToRefresh() {
        refreshControl?.beginRefreshing()
    }

    public func stopPullToRefresh() {
        self.refreshControl?.endRefreshing()
    }
    
    public func addLoadMore(action: @escaping (() -> Void)) {
        let size = CGSize(width: self.frame.size.width, height: loadMoreDefaultHeight)
        let frame = CGRect(origin: .zero, size: size)
        loadMoreView = LoadMoreView(action: action, frame: frame)
        loadMoreView?.autoresizingMask = [.flexibleWidth, .flexibleHeight]

        addSubview(loadMoreView!)
    }

    public func startLoadMore() {
        print("Start Load More")
        loadMoreView?.isLoading = true
    }

    public func stopLoadMore() {
        print("Stop Load More")
        loadMoreView?.isLoading = false
    }

    public func setLoadMoreEnable(_ enable: Bool) {
        loadMoreView?.isEnabled = enable
    }
    
    @objc private func refreshAction() {
        pullToRefreshListener?()
    }

}
